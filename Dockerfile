FROM ubuntu:18.04
WORKDIR /opt/core
COPY . /opt/core
RUN su root
RUN apt-get update
RUN chmod +x ./sql/setupmysql.sh
RUN ./sql/setupmysql.sh
RUN apt-get install -y nodejs npm
RUN npm i
EXPOSE 3500
RUN chmod +x start.sh
CMD ["/bin/sh","start.sh"]
