function main(serverState,socket,fs){
	if(serverState=='adRole' || serverState=='tour'){
		adRole(serverState,socket,fs);
	}else if(serverState=='static'){
		static(serverState,socket,fs);
	}
}

function adRole(serverState,socket,fs){
	const folder = '/adRole/';
	var files = [];
	fs.readdirSync('./public'+folder).forEach(file => {
		if(file.slice(-4)=='.jpg' || file.slice(-4)=='.png'){
	 		files.push(folder+file);
	 	}
	});
	socket.emit(serverState,{data:files});
}

function static(serverState,socket,fs){
	const folder = '/static/';
	var files = [];
	fs.readdirSync('./public'+folder).forEach(file => {
		if(file.slice(-4)=='.jpg' || file.slice(-4)=='.png'){
	 		files.push(folder+file);
	 	}
	});
	if(files.length>0){
		socket.emit(serverState,{data:files[0]});
	}
}

async function files(req,res,authToken,authFunction,atob,log,getNewConnection,fs){
	try{
		if(await authFunction(authToken,atob,log,getNewConnection,'member')){
			var files = [];
			var folder = req.query.folder || 'adRole';
			fs.readdirSync('./public/'+folder).forEach(file => {
				if(file.slice(-4)=='.jpg' || file.slice(-4)=='.png' || file.slice(-4)=='.mp4'){
			 		files.push(file);
			 	}
			});
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Authorized files request');
			res.send({'status':'success','data':files});
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized files request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.sendStatus(500);
	}
}

async function state(req,res,updateState,authFunction,authToken,atob,io,log,getNewConnection){
	try{	
		if(await authFunction(authToken,atob,log,getNewConnection,'member')){
			io.state=updateState;
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Authorized state request');
			io.emit('control',{data:updateState});
			res.send({"status":"success",statusCode:200});
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized state request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.sendStatus(500);
	}
}

function boards(req,res,io){
	try{
		res.send({"status":"success","data":io.videoInv});
	}catch(err){
		console.log(err);
		res.sendStatus(500);
	}
}

async function upload(req,res,directory,authFunction,atob,log,getNewConnection,path,fs){
	try{
		if(await authFunction(req.query.authorization||req.headers.authorization,atob,log,getNewConnection,'member')){
			log(JSON.parse(atob(req.query.authorization||req.headers.authorization)).username || 'unknown','Authorized upload request');
			var filePath = './public/'+req.query.type+'/'+req.files.file.name;
			req.files.file.mv(path.resolve(filePath),function(err){
				if(err){
					console.log(err);
					res.sendStatus(500);
				}else{
					if(req.query.type=='static'){
						try{
							var files = [];
							fs.readdirSync('./public/static').forEach(file => {
								if(file.slice(-4)=='.jpg' || file.slice(-4)=='.png'){
							 		files.push('./public/static/'+file);
							 	}
							});
							if(files.length>1){
								for(var i=0;i<files.length;i++){
									if(files[i]!=filePath){
										fs.unlinkSync(files[i]);
									}
								}
							}
						}catch(err){
							res.send({'status':'warning','data':'File Uploaded Successfully','error':'Could not delete other files in static directory!'})
						}
					}
					res.send({"status":"success"});
				}
			});
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized upload request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.sendStatus(500);
	}
}

async function deleteFile(req,res,authToken,authFunction,atob,log,getNewConnection,fs){
	try{
		if(await authFunction(authToken,atob,log,getNewConnection,'member')){
			log(JSON.parse(atob(authToken)).username || 'unknown','Authorized delete file request');
			if(req.body.type && req.body.file){
				fs.unlink('./public/'+req.body.type+'/'+req.body.file,function(err){
					if(err){
						res.status(500).send({'status':'error','error':'Error deleting file'});
					}else{
						res.send({'status':'success'});
					}
				});
			}else{
				res.status(500).send({'status':'error','error':'Missing parameters'});
			}
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized delete file request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.sendStatus(500);
	}
}

module.exports={
	main,
	state,
	boards,
	upload,
	files,
	deleteFile
} 