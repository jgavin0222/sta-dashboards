var sha512 = require('js-sha512').sha512;

//Auth key generation
const salt = '$5as*&*231AS';
const salt2 = '#fasd*#)@!';
const salt3 = 'S&^423ASHD732#)@@!!';

function auth(req,res,getNewConnection,log,btoa){
	try{
		var username = req.body.user;
		var encPassword = req.body.passwordHash;
		var hash = sha512(salt+encPassword+salt2);
		var connection = getNewConnection();
		var user = connection.escape(username);
		var password = connection.escape(hash);
		connection.connect(function(err){
			if(err){
				res.sendStatus(500);
			}
			connection.query('SELECT * FROM users WHERE username='+user+' AND password='+password,function(err,data){
				if(err){
					log(username,'Failed to authenticate');
					res.sendStatus(403);
				}else{
					connection.end();
					if(data.length>0){
						log(username,'Authentication success');
						var expiration = new Date();
						expiration.setHours(expiration.getDate()+24);
						res.send({'status':'success','jwt':btoa(JSON.stringify({'username':data[0].username,'authorization':data[0].authorization,'expiration':expiration.toString(),'signature':generateSignature(data[0].username,data[0].authorization,expiration.toString())}))});
					}else{
						res.sendStatus(403);
					}
				}
			});
		});
	}catch(err){
		console.log(err);
		res.status(500).send({'status':'error'});
	}
}

async function register(req,res,authToken,getNewConnection,log,atob){
	try{
		if(await authorize(authToken,atob,log,getNewConnection)){
			var connection = getNewConnection();
			var usernameN = connection.escape(req.body.user);
			var passwordN = connection.escape(sha512(salt+req.body.passwordHash+salt2));
			var authorizationN = connection.escape(req.body.authorization);
			connection.connect(function(err){
				if(err){
					res.sendStatus(500);
				}
				connection.query('INSERT INTO users (`username`,`password`,`authorization`) VALUES ('+usernameN+','+passwordN+','+authorizationN+')',function(err,data){
					connection.end();
					if(err){
						log(JSON.parse(atob(authToken)).username || 'unknown','Database error on registration request');
						res.sendStatus(500);
					}else{
						log(JSON.parse(atob(authToken)).username || 'unknown','Authorized registration request');
						res.send({'status':'success'});
					}
				});
			});
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized registration request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.status(500).send({'status':'error'});
	}
}

async function users(req,res,authToken,atob,log,getNewConnection){
	try{
		if(await authorize(authToken,atob,log,getNewConnection)){
			var connection = getNewConnection();
			connection.connect(function(err){
				if(err){
					res.status(500).send({'status':'error','error':'could not connect to database'});
				}
				connection.query('SELECT id,username,authorization FROM users',function(err,data){
					connection.end();
					if(err){
						log(JSON.parse(atob(authToken)).username || 'unknown','Database error on users request');
						res.sendStatus(500);
					}else{
						log(JSON.parse(atob(authToken)).username || 'unknown','Authorized users request');
						res.send({'status':'success','data':data});
					}
				});
			});
		}else{
			log(JSON.parse(atob(authToken)).username || 'unknown','Unauthorized users request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.status(500).send({'status':'error'});
	}
}

async function deleteUser(req,res,authToken,atob,log,getNewConnection){
	try{
		if(await authorize(authToken,atob,log,getNewConnection)){
			if(req.body.id){
				var connection = getNewConnection();
				var id = connection.escape(req.body.id);
				connection.connect(function(err){
					if(err){
						res.status(500).send({'status':'error','error':'could not connect to database'});
					}
					connection.query('DELETE FROM users where id='+id,function(err,data){
						connection.end();
						if(err){
							log(JSON.parse(atob(authToken)).username || 'unknown','Database error on delete user request');
							res.sendStatus(500);
						}else{
							log(JSON.parse(atob(authToken)).username || 'unknown','Authorized delete user request');
							res.send({'status':'success'});
						}
					});
				});
			}else{
				res.status(500).send({'status':'error','error':'id parameter missing'});
			}
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized delete user request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.status(500).send({'status':'error'});
	}
}

async function resetPass(req,res,authToken,atob,log,getNewConnection){
	try{
		if(await authorize(authToken,atob,log,getNewConnection)){
			if(req.body.id && req.body.passwordHash){
				var connection = getNewConnection();
				var encPassword = req.body.passwordHash;
				var hash = sha512(salt+encPassword+salt2);
				var id = connection.escape(req.body.id);
				var password = connection.escape(hash);
				connection.connect(function(err){
					if(err){
						res.status(500).send({'status':'error','error':'could not connect to database'});
					}
					connection.query('UPDATE users set password='+password+' WHERE id='+id,function(err,data){
						connection.end();
						if(err){
							log(JSON.parse(atob(authToken)).username || 'unknown','Database error on reset user password request');
							res.sendStatus(500);
						}else{
							log(JSON.parse(atob(authToken)).username || 'unknown','Authorized reset user password request');
							res.send({'status':'success'});
						}
					});
				});
			}else{
				res.status(500).send({'status':'error','error':'id or passwordHash parameter missing'});
			}
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Unauthorized reset user password request');
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.status(500).send({'status':'error'});
	}
}

function authorize(authorization,atob,log,getNewConnection,authReq){
	return new Promise((res,rej)=>{
		var connection = getNewConnection();
		connection.connect(function(err){
			if(err){
				res(false);
			}else{
				try{
					connection.query('Select authorization FROM users WHERE username='+connection.escape(JSON.parse(atob(authorization)).username),function(err,data){
						if(err){
							log(JSON.parse(atob(authorization)).username || 'Unauthorized authentication validation');
						}else{
							connection.end();
							if(data.length>0){
								var jwt = JSON.parse(atob(authorization));
								if(data[0].authorization===jwt.authorization && ((jwt.authorization == authReq)||(jwt.authorization == 'admin'))){
									var tokenSignature = generateSignature(jwt.username,jwt.authorization,jwt.expiration);
									if(tokenSignature === jwt.signature){
										var cDate = new Date(jwt.expiration);
										res(cDate>new Date());
									}
								}else{
									log(JSON.parse(atob(authorization)).username || 'Unauthorized authorization validation');
									res(false);
								}
							}else{
								log(JSON.parse(atob(authorization)).username || 'Unauthorized authentication validation');
								res(false);
							}
						}
					});
				}catch(err){
					rej(false);
				}
			}
		});
	});
}

function generateSignature(username,auth,expiration){
	return sha512(salt3+salt+username+auth+salt2+expiration.toString());
}

module.exports = {
	auth,
	register,
	authorize,
	users,
	deleteUser,
	resetPass
}