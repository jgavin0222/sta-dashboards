var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var mysql = require('mysql');
var auth = require('./auth');
var core = require('./core');
var path = require('path');
const fileUpload = require('express-fileupload');
var args = require('minimist')(process.argv.slice(2));
var fs = require('fs');

//Start the server
const port = args['port']||3500;
const dbHost = args['dbHost']||'localhost';
const dbPort = args['dbPort']||3306;
const dbUser = args['dbUser']||'sta';
const dbPass = args['dbPass']||'()2212!!35';
server.listen(port);

//Setup mysql
function getNewConnection(){
	return mysql.createConnection({
		host:dbHost,
		database:'STADashboard',
		user:dbUser,
		port:dbPort,
		password:dbPass,
		insecureAuth: true
	}); 
}

getNewConnection().connect(function(err){
	if(err){
		console.log(err);
		console.log('Could not log into the database @ '+dbHost+':'+dbPort+' as user '+dbUser);
		process.exit(1);
	}else{
		console.log('Connected to the database @ '+dbHost+":"+dbPort);
	}
});

//Log
console.log('Server is listening on port: '+port);
io.state = 'static';
io.videoInv = [];

app.use(fileUpload());
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/',function(req,res){
	res.sendFile(__dirname+'/public/index.html');
});
app.get(['/admin/:file','/admin/'],async(req,res)=>{
	if((req.query.authorization || req.headers.authorization) != null){
		if(await auth.authorize(req.query.authorization||req.headers.authorization,atob,log,getNewConnection)){
			res.sendFile(__dirname+'/admin/'+req.params.file);
		}else{
			res.sendFile(__dirname+'/admin/admin.html');
		}
	}else{
		res.sendFile(__dirname+'/admin/admin.html')
	}
});
app.get(['/control/:file','/control/'],async(req,res)=>{
	if((req.query.authorization || req.headers.authorization) != null){
		if(await auth.authorize(req.query.authorization||req.headers.authorization,atob,log,getNewConnection,'member')){
			res.sendFile(__dirname+'/control/'+req.params.file);
		}else{
			res.sendFile(__dirname+'/control/control.html');
		}
	}else{
		res.sendFile(__dirname+'/control/control.html')
	}
});
app.get('/boards',(req,res)=>core.boards(req,res,io));
app.get('/users',(req,res)=>auth.users(req,res,req.headers.authorization,atob,log,getNewConnection));
app.get('/files',(req,res)=>core.files(req,res,req.headers.authorization,auth.authorize,atob,log,getNewConnection,fs))

app.post('/auth',(req,res)=>auth.auth(req,res,getNewConnection,log,btoa));
app.post('/delete',(req,res)=>auth.deleteUser(req,res,req.headers.authorization,atob,log,getNewConnection));
app.post('/deleteFile',(req,res)=>core.deleteFile(req,res,req.headers.authorization,auth.authorize,atob,log,getNewConnection,fs));
app.post('/register',(req,res)=>auth.register(req,res,req.headers.authorization,getNewConnection,log,atob));
app.post('/resetpass',(req,res)=>auth.resetPass(req,res,req.headers.authorization,atob,log,getNewConnection));
app.post('/state',(req,res)=>core.state(req,res,req.body.state,auth.authorize,req.headers.authorization,atob,io,log,getNewConnection));
app.post('/upload',(req,res)=>core.upload(req,res,req.body.directory,auth.authorize,atob,log,getNewConnection,path,fs));

app.post('/emit',async function(req,res){
	try{
		if(await auth.authorize(req.headers.authorization,atob,log,getNewConnection)){
			if(req.body.topic=='videoInv'){
				for(var i=0;i<req.body.boards.length;i++){
					io.emit(req.body.topic,{data:req.body.data,videoName:req.body.boards[i]});
				}
			}else{
				io.emit(req.body.topic,{data:req.body.data});
			}
			log(JSON.parse(atob(req.headers.authorization)).username || 'unknown','Authorized emit request');
			res.send({'status':'success'});
		}else{
			log(JSON.parse(atob(req.headers.authorization)).username || 'anonymous','Unauthorized emit request')
			res.sendStatus(403);
		}
	}catch(err){
		console.log(err);
		res.send({'status':'error','statusCode':500,'stackTrace':err});
	}
});

function btoa(string){
	return new Buffer(string).toString('base64');
}
function atob(string){
	return new Buffer.from(string,'base64').toString('utf8');
}

function log(username,log){
	try{
		var connection = getNewConnection();
		connection.connect(function(err){
			if(err){
				console.log(err);
			}else{
				connection.query('INSERT INTO logs (`username`,`event`,`timestamp`) VALUES ("'+username+'","'+log+'",NOW())',function(err){
					if(err){
						console.log(err,'Failed to log user ('+username+') event of '+log);
					}else{
						console.log('Successfuly logged user ('+username+') event of '+log);
					}
				});
			}
		});
		connection.end();
	}catch(err){
		console.log(err,'Failed to log user ('+username+') event of '+log);
	}
}

io.on('connection',function(socket){
	var videoInv;

	console.log('Socket connected');
	socket.join('control');
	socket.emit('control',{data:io.state});

	socket.on('get',function(){
		core.main(io.state,socket,fs);
	})

	socket.on('subscribe',function(msg){
		if(msg.data=='videoInv'){
			var videoName = msg.videoName;
			if(io.videoInv.indexOf(msg.videoName)==-1){
				io.videoInv.push(msg.videoName);
				videoInv = msg.videoName;
			}
		}else{
			socket.join(msg.data);
		}
	});
	socket.on('unsubscribe',function(msg){
		socket.leave(msg.data);
	});
	socket.on('disconnect',function(){
		if(io.videoInv.indexOf(videoInv)!=-1){
			io.videoInv.splice(io.videoInv.indexOf(videoInv),1);
		}
		console.log('Socket disconnected');
	});
});